import React from 'react';
import { TouchableOpacity, View, Image } from 'react-native';

const Header = () => {
  <View style={styles.container}>
    <TouchableOpacity style={styles.button}>
      <Image style={styles.icon} source={require('./images/gear.png')} />
    </TouchableOpacity>
  </View>
};

export default Header;
