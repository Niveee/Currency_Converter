import React, { Component } from 'react';
import {
  StyleSheet,
  Button,
  Text,
  View,
  TextInput,
  TouchableOpacity
} from 'react-native';

const styles = StyleSheet.create({
  textInput: {
    height: 40,
    borderColor: 'silver',
    borderBottomWidth: 1,
    marginBottom:10,
    borderRadius: 25,
    backgroundColor:'#E6E6E6'
  },
  loginCard: {
    marginTop:200,
    marginLeft:70,
    padding:10,
    width:280
  },
  loginButton: {
    borderRadius: 25,
  }
});


export default class Login extends Component {

  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: ''
    };
  }

  render() {
    return (
      <View>
        <View style = {styles.loginCard}>
          <TextInput
            style = {styles.textInput}
            onChangeText = {(username) => this.setState({username})}
            value = {this.state.username}
            placeholder = "Email"
            underlineColorAndroid={'transparent'}
            keyboardType='email-address'
          />
          <TextInput
            style = {styles.textInput}
            onChangeText = {(password) => this.setState({password})}
            value = {this.state.password}
            placeholder = "Password"
            underlineColorAndroid={'transparent'}
            secureTextEntry={true}
          />
            <TouchableOpacity >
              <Button
                title="Login"
                color="#57B846"
                style={styles.loginButton}
              />
            </TouchableOpacity>
        </View>
      </View>
    );
  }
}
