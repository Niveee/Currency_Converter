import React from 'react';
import PropTypes from 'prop-types';
import { View, Text } from 'react-native';
import moment from 'moment';

import styles from './styles';

const LastConverted = ({ date, base, quote, conversionRate }) => {
  <View>
    <Text style={styles.smallText}>
      1 {base} = {conversionRate} {quote} as of {moment(date).format('MMMM D, YYY')}
    </Text>
  </View>
};

LastConverted.propTypes = {
  date: PropTypes.object,
  base: PropTypes.string,
  quote: PropTypes.string,
  conversionRate: PropTypes.number
}

export default LastConverted;
