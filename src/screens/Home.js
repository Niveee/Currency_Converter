import React, { Component } from 'react';
import { View, StatusBar } from 'react-native';

import { Container } from '../components/Container';
import { Logo } from '../components/Logo';
import { InputWithButton } from '../components/TextInput';
import { ClearButton } from '../components/Buttons';
import { LastConverted } from '../components/Text';
import { Header } from '../components/Header';

const TEMP_BASE_CURRENCY = 'USD' ;
const TEMP_QUOTE_CURRENCY = 'GDP' ;
const TEMP_BASE_PRICE = '100' ;
const TEMP_QUOTE_PRICE = '79.74' ;
const TEMP_CONVERSION_RATE = 0.7974;
const TEMP_CONVERSION_DATE = new Date();

class Home extends Component {

  handlePressBaseCurrency = () => {
    console.log("press base");
  };

  handlePressQuoteCurrency = () =>  {
    console.log("press quote");
  };

  handleChangeText = () => {
    console.log("change text");
  };

  handleSwapCurrency = () => {
    console.log("press swap currency");
  }

  render() {
    return (
      <Container >
        <StatusBar translucent={false}/>
        <Header />
        <Logo />
        <InputWithButton
          buttonText={TEMP_BASE_CURRENCY}
          onPress={this.handlePressBaseCurrency}
          defaultValue={TEMP_BASE_PRICE}
          keyboardType="numeric"
          onChange={this.handleChangeText}
        />
        <InputWithButton
          buttonText={TEMP_QUOTE_CURRENCY}
          onPress={this.handlePressQuoteCurrency}
          editable={false}
          value={TEMP_QUOTE_PRICE}
        />
        <ClearButton
          onPress={this.handleSwapCurrency}
          text="Reverse currencies"
        />
        <LastConverted
          base={TEMP_BASE_CURRENCY}
          quote={TEMP_QUOTE_CURRENCY}
          date={TEMP_CONVERSION_DATE}
          conversionRate={TEMP_CONVERSION_RATE}
        />
      </Container>
    );
  }
}

export default Home;
